import com.twaran.parkingLot.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ParkingLotTest {
    @Test
    void shouldReturnAvailableWhenThereIsAParkingLot() throws CarAlreadyExist, ParkingLotFullException {
        ParkingLot parkingLot = new ParkingLot(10);
        Vehicle car = new Vehicle() {};

        boolean isCarParked = parkingLot.park(car);

        assertTrue(isCarParked);
    }

    @Test
    void shouldParkingLotFullExceptionWhenThereIsNoParkingLot() throws CarAlreadyExist, ParkingLotFullException {
        ParkingLot parkingLot = new ParkingLot(1);
        Vehicle car1 = new Vehicle() {};
        Vehicle car2 = new Vehicle() {};

        parkingLot.park(car1);

        assertThrows(ParkingLotFullException.class, () -> parkingLot.park(car2));
    }

    @Test
    void shouldThrowCarAlreadyExistExceptionWhenSameCarIsParked() throws CarAlreadyExist, ParkingLotFullException {
        ParkingLot parkingLot = new ParkingLot(2);
        Vehicle car1 = new Vehicle() {};

        parkingLot.park(car1);

        assertThrows(CarAlreadyExist.class, () -> parkingLot.park(car1));
    }

    @Test
    void shouldUnParkTheCarWhenIsInParkingLot() throws CarAlreadyExist, ParkingLotFullException, NoSuchCarInParkinglot {
        ParkingLot parkingLot = new ParkingLot(2);
        Vehicle car1 = new Vehicle() {};

        parkingLot.park(car1);
        boolean isCar1UnParked = parkingLot.unPark(car1);

        assertTrue(isCar1UnParked);
    }

    @Test
    void shouldThrowNoSuchCarInParkingLotExceptionWhenTheCarIsNotInParkingLot() {
        ParkingLot parkingLot = new ParkingLot(2);
        Vehicle car1 = new Vehicle() {};

        assertThrows(NoSuchCarInParkinglot.class, () -> parkingLot.unPark(car1));
    }
}
