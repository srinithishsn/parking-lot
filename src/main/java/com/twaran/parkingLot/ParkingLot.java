package com.twaran.parkingLot;

import java.util.HashSet;

public class ParkingLot {
    private final int capacity;

    HashSet<Vehicle> cars;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        this.cars = new HashSet<>(capacity);
    }

    public boolean park(Vehicle car) throws ParkingLotFullException, CarAlreadyExist {
        if (isAlreadyParked(car))
            throw new CarAlreadyExist();
        if (cars.size() < capacity) {
            cars.add(car);
            return true;
        }
        throw new ParkingLotFullException();
    }

    private boolean isAlreadyParked(Vehicle car) {
        return cars.contains(car);
    }

    public boolean unPark(Vehicle car) throws NoSuchCarInParkinglot {
        if (isAlreadyParked(car)) {
            cars.remove(car);
            return true;
        }
        throw new NoSuchCarInParkinglot();
    }
}
